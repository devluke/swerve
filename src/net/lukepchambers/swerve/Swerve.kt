package net.lukepchambers.swerve

import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.*
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.io.InputStreamReader
import java.io.BufferedReader
import java.net.URL

@Suppress("unused")
class Swerve: JavaPlugin(), Listener {

    private val console = server.consoleSender!!
    private val pm = server.pluginManager!!

    private val separator = File.separator

    private val serverDirectory = File("$dataFolder$separator..$separator..")

    private val dataConfigFile = File("$dataFolder${separator}data.yml")
    private lateinit var dataConfig: YamlConfiguration

    private val configVersion = 13

    private val msg = mutableMapOf<String, String>()
    private lateinit var prefix: String

    override fun onEnable() {

        val tPrefix = "${ChatColor.DARK_AQUA}${ChatColor.BOLD}SW ${ChatColor.AQUA}»${ChatColor.GRAY}"

        console.sendMessage("$tPrefix Enabling Swerve version ${description.version}...")

        pm.registerEvents(this, this)

        console.sendMessage("$tPrefix Loading configuration files...")

        saveDefaultConfig()

        if (config.getInt("version") < 13) {

            console.sendMessage("$tPrefix Migrating to Swerve 1.0.1...")

            File("$dataFolder${separator}worlds").deleteRecursively()
            File("$dataFolder${separator}messages.yml").delete()

            val tempDataConfig = lc(dataConfigFile)

            tempDataConfig.set("version", null)
            tempDataConfig.save(dataConfigFile)

        }

        if (config.getInt("version") < configVersion) {

            console.sendMessage("$tPrefix Updating config.yml...")

            File("$dataFolder${separator}config.yml").delete()

            saveDefaultConfig()
            reloadConfig()

        }

        if (!dataConfigFile.exists()) {

            console.sendMessage("$tPrefix Creating data.yml...")

            dataConfigFile.createNewFile()

            dataConfig = lc(dataConfigFile)

        }

        dataConfig = lc(dataConfigFile)

        console.sendMessage("$tPrefix Reading messages from config.yml...")

        val messages = config.getConfigurationSection("messages").getKeys(false)
        prefix = ChatColor.translateAlternateColorCodes('&', config.getString("messages.prefix"))

        messages.forEach {

            val unformattedString = "$prefix${config.getString("messages.$it")}"

            msg[it] = ChatColor.translateAlternateColorCodes('&', unformattedString)

        }

        console.sendMessage("$tPrefix Plugin ready, preparing world configuration files...")

        console.sendMessage("$tPrefix Creating new world and updating old world configuration sections...")

        serverDirectory.listFiles().filter { it.isDirectory }.forEach {

            if (File("${it.path}${separator}level.dat").exists() && server.getWorld(it.name) != null) {

                if (config.getConfigurationSection("worlds.${it.name}") == null) {

                    console.sendMessage("$tPrefix Creating config.yml/worlds.${it.name}...")

                    config.set("worlds.${it.name}.perworld.commands.mode", 1)
                    config.set("worlds.${it.name}.perworld.commands.commands", listOf("command1", "command2"))

                    saveConfig()

                }

            }

        }

        console.sendMessage("$tPrefix Swerve version ${description.version} has been enabled.")

        if (config.getBoolean("options.update-checking")) {

            console.sendMessage("$tPrefix Checking for updates...")

            if (updateAvailable()) {

                console.sendMessage("$tPrefix A Swerve update is available. (${description.version} -> ${latestVersion()})")

            } else console.sendMessage("$tPrefix You're using the latest version of Swerve.")

        }

    }

    override fun onDisable() {

        val tPrefix = "${ChatColor.DARK_AQUA}${ChatColor.BOLD}SW ${ChatColor.AQUA}»${ChatColor.GRAY}"

        console.sendMessage("$tPrefix Swerve version ${description.version} has been disabled.")

    }

    override fun onCommand(sender: CommandSender, cmd: Command, label: String, args: Array<String>): Boolean {

        val command = cmd.label
        val isPlayer = sender is Player

        val world = if (isPlayer) (sender as Player).world else null

        val usage = server.getPluginCommand(command).usage

        val permission = "swerve.cmd.$command"
        val otherPermission = "$permission.others"

        if (!sender.hasPermission("swerve.cmd.$command")) {

            sender.sendMessage(msg["no-permission"])
            sender.sendMessage("$prefix$permission")

            return true

        }

        if (command == "swerve") {

            if (args.isNotEmpty()) {

                sender.sendMessage(msg["invalid-arguments"])
                sender.sendMessage("$prefix$usage")

            }

            sender.sendMessage("${prefix}Swerve version ${description.version} by TheLukeGuy.")

            return true

        }

        if (command == "spawn") {

            if (args.size !in 0..1 || (!isPlayer && args.size != 1)) {

                sender.sendMessage(msg["invalid-arguments"])
                sender.sendMessage("$prefix$usage")

                return true

            }

            if (args.size == 1 && !sender.hasPermission("swerve.cmd.spawn.others")) {

                sender.sendMessage(msg["no-permission-others"])
                sender.sendMessage("$prefix$otherPermission")

                return true

            }

            lateinit var location: Location
            lateinit var player: Player

            when (args.size) {

                0 -> {

                    player = sender as Player

                    if (!config.getBoolean("perworld.teleports")) {

                        sender.sendMessage(msg["teleports-not-enabled"])

                        return true

                    }

                    val dataPath = "data.worlds.${world!!.name}.teleports.spawn"

                    if (!dataConfig.contains(dataPath)) {

                        sender.sendMessage(msg["spawn-not-defined"])

                        return true

                    }

                    val x = dataConfig.getDouble("$dataPath.x")
                    val y = dataConfig.getDouble("$dataPath.y")
                    val z = dataConfig.getDouble("$dataPath.z")
                    val yaw = dataConfig.getDouble("$dataPath.yaw").toFloat()
                    val pitch = dataConfig.getDouble("$dataPath.pitch").toFloat()

                    location = Location(world, x, y, z, yaw, pitch)

                }

                1 -> {

                    player = server.getPlayer(args[0])

                    if (player == null) {

                        sender.sendMessage(msg["player-not-online"])

                        return true

                    }

                    val playerWorld = player.world

                    if (!config.getBoolean("perworld.teleports")) {

                        sender.sendMessage(msg["teleports-not-enabled"])

                        return true

                    }

                    val dataPath = "data.worlds.${playerWorld.name}.teleports.spawn"

                    if (!dataConfig.contains(dataPath)) {

                        sender.sendMessage(msg["spawn-not-defined"])

                        return true

                    }

                    val x = dataConfig.getDouble("$dataPath.x")
                    val y = dataConfig.getDouble("$dataPath.y")
                    val z = dataConfig.getDouble("$dataPath.z")
                    val yaw = dataConfig.getDouble("$dataPath.yaw").toFloat()
                    val pitch = dataConfig.getDouble("$dataPath.pitch").toFloat()

                    location = Location(playerWorld, x, y, z, yaw, pitch)

                }

            }

            player.teleport(location)

            return true

        }

        if (command == "setspawn") {

            if (!isPlayer) {

                sender.sendMessage(msg["must-be-player"])

                return true

            }

            if (args.isNotEmpty()) {

                sender.sendMessage(msg["invalid-arguments"])
                sender.sendMessage("$prefix$usage")

                return true

            }

            val location = (sender as Player).location

            val dataPath = "data.worlds.${sender.world.name}.teleports.spawn"

            dataConfig.set("$dataPath.x", location.x)
            dataConfig.set("$dataPath.y", location.y)
            dataConfig.set("$dataPath.z", location.z)
            dataConfig.set("$dataPath.yaw", location.yaw)
            dataConfig.set("$dataPath.pitch", location.pitch)

            dataConfig.save(dataConfigFile)

            sender.sendMessage(msg["spawn-set"])

            return true

        }

        if (command == "warp") {

            if (args.size !in 1..2 || (!isPlayer && args.size != 2)) {

                sender.sendMessage(msg["invalid-arguments"])
                sender.sendMessage("$prefix$usage")

                return true

            }

            if (args.size == 2 && !sender.hasPermission("swerve.cmd.warp.others")) {

                sender.sendMessage(msg["no-permission-others"])
                sender.sendMessage("$prefix$otherPermission")

                return true

            }

            lateinit var location: Location
            lateinit var player: Player

            when (args.size) {

                1 -> {

                    player = sender as Player

                    if (!config.getBoolean("perworld.teleports")) {

                        sender.sendMessage(msg["teleports-not-enabled"])

                        return true

                    }

                    val dataPath = "data.worlds.${world!!.name}.teleports.warps.${args[0]}"

                    if (!dataConfig.contains(dataPath)) {

                        sender.sendMessage(msg["warp-not-defined"])

                        return true

                    }

                    val warpPermission = "swerve.warps.${args[0]}"

                    if (config.getBoolean("options.warp-permissions") && !sender.hasPermission(warpPermission)) {

                        sender.sendMessage(msg["no-permission"])
                        sender.sendMessage("$prefix$warpPermission")

                        return true

                    }

                    val x = dataConfig.getDouble("$dataPath.x")
                    val y = dataConfig.getDouble("$dataPath.y")
                    val z = dataConfig.getDouble("$dataPath.z")
                    val yaw = dataConfig.getDouble("$dataPath.yaw").toFloat()
                    val pitch = dataConfig.getDouble("$dataPath.pitch").toFloat()

                    location = Location(world, x, y, z, yaw, pitch)

                }

                2 -> {

                    player = server.getPlayer(args[1])

                    if (player == null) {

                        sender.sendMessage(msg["player-not-online"])

                        return true

                    }

                    val playerWorld = player.world

                    if (!config.getBoolean("perworld.teleports")) {

                        sender.sendMessage(msg["teleports-not-enabled"])

                        return true

                    }

                    val dataPath = "data.worlds.${playerWorld.name}.teleports.warps.${args[0]}"

                    if (!dataConfig.contains(dataPath)) {

                        sender.sendMessage(msg["warp-not-defined"])

                        return true

                    }

                    val x = dataConfig.getDouble("$dataPath.x")
                    val y = dataConfig.getDouble("$dataPath.y")
                    val z = dataConfig.getDouble("$dataPath.z")
                    val yaw = dataConfig.getDouble("$dataPath.yaw").toFloat()
                    val pitch = dataConfig.getDouble("$dataPath.pitch").toFloat()

                    location = Location(playerWorld, x, y, z, yaw, pitch)

                }

            }

            player.teleport(location)

            return true

        }

        if (command == "setwarp") {

            if (!isPlayer) {

                sender.sendMessage(msg["must-be-player"])

                return true

            }

            if (args.size != 1) {

                sender.sendMessage(msg["invalid-arguments"])
                sender.sendMessage("$prefix$usage")

                return true

            }

            val location = (sender as Player).location

            val dataPath = "data.worlds.${sender.world.name}.teleports.warps.${args[0]}"

            dataConfig.set("$dataPath.x", location.x)
            dataConfig.set("$dataPath.y", location.y)
            dataConfig.set("$dataPath.z", location.z)
            dataConfig.set("$dataPath.yaw", location.yaw)
            dataConfig.set("$dataPath.pitch", location.pitch)

            dataConfig.save(dataConfigFile)

            sender.sendMessage(msg["warp-set"])

            return true

        }

        if (command == "delwarp") {

            if (!isPlayer) {

                sender.sendMessage(msg["must-be-player"])

                return true

            }

            if (args.size != 1) {

                sender.sendMessage(msg["invalid-arguments"])
                sender.sendMessage("$prefix$usage")

                return true

            }

            val dataPath = "data.worlds.${(sender as Player).world.name}.teleports.warps.${args[0]}"

            if (!dataConfig.contains(dataPath)) {

                sender.sendMessage(msg["warp-not-defined"])

                return true

            }

            dataConfig.set(dataPath, null)

            sender.sendMessage(msg["warp-deleted"])

            return true

        }

        if (command == "warps") {

            if (!isPlayer) {

                sender.sendMessage(msg["must-be-player"])

                return true

            }

            if (args.isNotEmpty()) {

                sender.sendMessage(msg["invalid-arguments"])
                sender.sendMessage("$prefix$usage")

                return true

            }

            val dataPath = "data.worlds.${(sender as Player).world.name}.teleports.warps"

            val warpSection = dataConfig.getConfigurationSection(dataPath)

            if (warpSection == null) {

                sender.sendMessage(msg["no-warps"])

                return true

            }

            val warps = warpSection.getKeys(false)

            var warpString = ""

            var warpCount = 0

            warps.forEach {

                if (!config.getBoolean("options.warp-permissions") || sender.hasPermission("swerve.warps.$it")) {

                    warpString += ", $it"

                    warpCount++

                }

            }

            if (warpCount == 0) {

                sender.sendMessage(msg["no-warps"])

                return true

            }

            sender.sendMessage("$prefix${warpString.removePrefix(", ")}")

            return true

        }

        return false

    }

    @EventHandler
    fun onAsyncPlayerChat(e: AsyncPlayerChatEvent) {

        if (e.player.hasPermission("swerve.perworld.chat.bypass")) return

        server.onlinePlayers.forEach {

            if (it.world != e.player.world && !it.hasPermission("swerve.perworld.chat.bypass")) e.recipients.remove(it)

        }

    }

    @Suppress("MemberVisibilityCanBePrivate")
    @EventHandler
    fun onPlayerChangedWorld(e: PlayerChangedWorldEvent) {

        val toName = e.player.world.name
        val fromName = e.from.name

        // Change hidden players

        if (config.getBoolean("perworld.teleports")) {

            server.onlinePlayers.forEach {

                e.player.showPlayer(this, it)
                it.showPlayer(this, e.player)

                if (e.player.world != it.world) {

                    val permission = "swerve.perworld.players.bypass"

                    if (!e.player.hasPermission(permission) && !it.hasPermission(permission)) {

                        e.player.hidePlayer(this, it)
                        it.hidePlayer(this, e.player)

                    }

                }

            }

        }

        // Save inventory

        val inventory = e.player.inventory

        var dataPath = "data.worlds.$fromName.players.${e.player.uniqueId}.inventory.contents"

        saveInventoryContentsToYaml(inventory, dataConfigFile, dataPath)

        // Load inventory

        val hasInvBypass = e.player.hasPermission("swerve.perworld.inventories.bypass")

        if (hasInvBypass) return

        if (!config.getBoolean("perworld.inventories")) return

        dataPath = "data.worlds.$toName.players.${e.player.uniqueId}.inventory.contents"

        val attemptContents = getInventoryContentsFromYaml(dataConfigFile, dataPath)

        if (attemptContents == null) e.player.inventory.clear()
        else e.player.inventory.contents = attemptContents

    }

    @EventHandler
    fun onPlayerCommandPreprocess(e: PlayerCommandPreprocessEvent) {

        if (e.player.hasPermission("swerve.perworld.commands.bypass")) return

        if (!config.getBoolean("perworld.commands")) return

        val commands = config.getStringList("worlds.${e.player.world.name}.perworld.commands.commands")
        val command = e.message.split(" ")[0].removePrefix("/")

        val mode = config.getInt("${e.player.world.name}.perworld.commands.mode")

        if ((mode == 1 && command in commands) || (mode == 2 && command !in commands)) {

            e.isCancelled = true

            val spigotYml = lc(File("${serverDirectory.path}${separator}spigot.yml"))
            val unknownMessage = spigotYml.getString("messages.unknown-command")

            e.player.sendMessage(ChatColor.translateAlternateColorCodes('&', unknownMessage))

        }

    }

    @EventHandler
    fun onPlayerJoin(e: PlayerJoinEvent) {

        // Update checking

        if (config.getBoolean("options.update-checking") && updateAvailable() && e.player.isOp) {

            e.player.sendMessage("${prefix}A Swerve update is available. (${description.version} -> ${latestVersion()})")

        }

        // Hide players

        if (config.getBoolean("perworld.players")) {

            server.onlinePlayers.forEach {

                if (e.player.world != it.world) {

                    val permission = "swerve.perworld.players.bypass"

                    if (!e.player.hasPermission(permission) && !it.hasPermission(permission)) {

                        e.player.hidePlayer(this, it)
                        it.hidePlayer(this, e.player)

                    }

                }

            }

        }

        // Load inventory

        val hasInvBypass = e.player.hasPermission("swerve.perworld.inventories.bypass")

        if (hasInvBypass) return

        if (!config.getBoolean("perworld.inventories")) return

        val dataPath = "data.worlds.${e.player.world.name}.players.${e.player.uniqueId}.inventory.contents"

        val attemptContents = getInventoryContentsFromYaml(dataConfigFile, dataPath)

        if (attemptContents == null) e.player.inventory.clear()
        else e.player.inventory.contents = attemptContents

    }

    @EventHandler
    fun onPlayerQuit(e: PlayerQuitEvent) {

        // Show all players

        server.onlinePlayers.forEach {

            e.player.showPlayer(this, it)
            it.showPlayer(this, e.player)

        }

        // Save inventory

        val inventory = e.player.inventory

        val dataPath = "data.worlds.${e.player.world.name}.players.${e.player.uniqueId}.inventory.contents"

        saveInventoryContentsToYaml(inventory, dataConfigFile, dataPath)

    }

    private fun saveInventoryContentsToYaml(inventory: Inventory, yamlConfigFile: File, yamlContentsPath: String) {

        val yamlConfig = lc(yamlConfigFile)
        val contents = inventory.contents

        var forCount = 0

        contents.forEach {

            yamlConfig.set("$yamlContentsPath.$forCount", it ?: "null")

            forCount++

        }

        yamlConfig.save(yamlConfigFile)

    }

    private fun getInventoryContentsFromYaml(yamlConfigFile: File, yamlContentsPath: String): Array<ItemStack?>? {

        val yamlConfig = lc(yamlConfigFile)
        val contents = arrayListOf<ItemStack?>()
        val savedItemsSection = yamlConfig.getConfigurationSection(yamlContentsPath) ?: return null

        val savedItems = savedItemsSection.getKeys(false)

        savedItems.forEach {

            val path = "$yamlContentsPath.$it"

            val tryGet = yamlConfig.get(path)

            contents.add(if (tryGet is String && tryGet == "null") null else yamlConfig.getItemStack(path))

        }

        return contents.toArray(arrayOfNulls<ItemStack>(contents.size))

    }

    private fun stringUpToDate(testVersion: String, currentVersion: String): Boolean {

        val testInt = testVersion.replace(".", "").toInt()
        val currentInt = currentVersion.replace(".", "").toInt()

        return testInt >= currentInt

    }

    private fun latestVersion(): String {

        val checkURL = URL("https://api.spigotmc.org/legacy/update.php?resource=58279")

        val connection = checkURL.openConnection()

        return BufferedReader(InputStreamReader(connection.getInputStream())).readLine()

    }

    private fun updateAvailable() = !stringUpToDate(description.version, latestVersion())

    private fun lc(file: File) = YamlConfiguration.loadConfiguration(file)

}